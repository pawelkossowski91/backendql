
const { GraphQLServer } = require('graphql-yoga')

// getting-started.js - to  makee conection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

const Todo = mongoose.model('Todo', {
    text: String,
    complete: Boolean,
});


const typeDefs = `
  type Query {
    hello(name: String): String!
    todos:[Todo]
  }
  type Todo {
      id: ID!
      text: String!
      complete: Boolean!
  }
  type Mutation {
      createTodo(text: String!): Todo
      updateTodo(id: ID!, complete: Boolean!): Boolean
      removeTodo(id: ID!, complete: Boolean!): Boolean
  }
`;

const resolvers = {
    Query: {
        hello: (_, { name }) => `Hello ${name || 'World'}`,
        todos: ()=>Todo.find()
    },
    Mutation: {
        createTodo: async (_, { text }) => {
            const todo = new Todo({text,complete: false});
            await todo.save();
            // todo.save();
            return todo;
        },
        updateTodo: async (_, {id, complete}) => {
            await Todo.findByIdAndUpdate(id,{complete});
        //    Todo.findByIdAndUpdate(id,{complete});
            return true;
        },
        removeTodo: async (_, {id}) => {
            await Todo.findByIdAndRemove(id);
        //    Todo.findByIdAndUpdate(id,{complete});
            return true;
        },
    // mutation end
    }
};

const server = new GraphQLServer({ typeDefs, resolvers })
// callback
mongoose.connection.once('open', function() {
    // we're connected!
//localhost
server.start(() => console.log('Server is running on localhost:4000'))

  });

